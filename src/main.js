import Vue from "vue"
import App from "./App.vue"
import store from "@/store"
import "bootstrap/dist/js/bootstrap.min.js"
import "bootstrap/dist/css/bootstrap.min.css"
import "nprogress/nprogress.js"
import "nprogress/nprogress.css"

// http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=DTU%0A
console.log(`
██████╗ ████████╗██╗   ██╗
██║  ██╗╚══██╔══╝██║   ██║ Created by Genomic Epidemiology group of National Food Institude.
██║  ██║   ██║   ██║   ██║ https://www.food.dtu.dk/english/Research/Genomic-Epidemiology
██████╔╝   ██║    ██████╔╝
╚═════╝    ╚═╝    ╚═════╝
`) // eslint-disable-line no-console

Vue.config.productionTip = false
Vue.prototype.$appName = "NTCounter"

new Vue({
  store,
  render: h => h(App)
}).$mount("#app")
