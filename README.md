# Installation

```bash
$ git clone --recursive git@bitbucket.org:genomicepidemiology/phenex-ntcounter-frontend.git
$ cd phenex-ntcounter-frontend
$ docker-compose -f composer-development.yaml build
$ docker-compose -f composer-development.yaml up
$ docker inspect phenex_dev_ntcounter_frontend_v1 | grep IPAddress | tail -n1
"IPAddress": "172.18.0.2"

# application served in development mode
$ curl 172.18.0.2:8080

# application in production
$ curl 172.18.0.2
```
