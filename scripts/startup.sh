#!/usr/bin/env ash

ln -s /app/scripts/aliases.sh /etc/profile.d/aliases.sh

## NGINX
rm -f /etc/nginx/conf.d/default.conf
cp /app/config/services/nginx.conf /etc/nginx/nginx.conf
cp /app/config/services/app.nginx.development.conf /etc/nginx/conf.d/app.conf

# NodeJs tasks
yarn install
yarn run build
cp -r /app/dist/* /usr/share/nginx/html

if [ "$DEPLOYMENT" = "development" ]; then
  npm run serve &
fi

## START WEB_SERVER IN FOREGROUND
nginx -g "daemon off;"
