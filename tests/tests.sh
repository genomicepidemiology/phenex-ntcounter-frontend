#!/bin/ash

set -e

# Check if server runs uwsgi server
server=$(curl -s localhost | grep "app")
if [ -z "$server" ]; then
  echo 'Server is not serving application.'
  exit 1
fi
