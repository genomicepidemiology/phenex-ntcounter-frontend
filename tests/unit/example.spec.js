import { shallowMount } from "@vue/test-utils";
import Upload from "@/components/Upload.vue";

describe("Upload.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(Upload, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
